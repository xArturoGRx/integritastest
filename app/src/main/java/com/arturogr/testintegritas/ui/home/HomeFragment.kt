package com.arturogr.testintegritas.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import coil.load
import com.arturogr.testintegritas.databinding.FragmentHomeBinding
import com.arturogr.testintegritas.utils.stringToBitMap
import com.yanzhenjie.album.Album
import com.yanzhenjie.album.AlbumConfig
import com.yanzhenjie.album.AlbumFile
import com.yanzhenjie.album.AlbumLoader
import java.util.Date

class HomeFragment : Fragment(), AlbumLoader {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private val images = ArrayList<AlbumFile>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        Album.initialize(AlbumConfig.newBuilder(requireContext()).setAlbumLoader(this).build())
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        binding.buttonTake.setOnClickListener {
            val path: String = requireContext().getExternalFilesDir(null).toString() + "/${Date().time}.jpg"
            Album.camera(requireActivity()) // Camera function.
                .image() // Take Picture.
                .filePath(path)
                .onResult {
                    homeViewModel.setPhoto(it)
                    val bitMap = it.stringToBitMap(requireContext())
                    binding.photoImage.load(bitMap)
                }
                .onCancel {
                }.start()
        }

        binding.buttonUpload.setOnClickListener {
            Album.image(this) // Image selection.
                .multipleChoice()
                .columnCount(3)
                .selectCount(1)
                .checkedList(images)
                .onResult { album ->
                    album.map {
                        val bitMap = it.path.stringToBitMap(requireContext())
                        binding.photoImage.load(bitMap)
                    }
                }
                .onCancel { }.start()
        }

        return root
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun load(imageView: ImageView?, albumFile: AlbumFile?) {
        if (albumFile != null) {
            load(imageView, albumFile.path)
        }
    }

    override fun load(imageView: ImageView?, url: String?) {
        val bitMap = url?.stringToBitMap(requireContext())
        imageView?.load(bitMap)
    }

}