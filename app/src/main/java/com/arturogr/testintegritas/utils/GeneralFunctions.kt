package com.arturogr.testintegritas.utils

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import android.provider.Settings

class GeneralFunctions {
    companion object {
        var storage = mutableListOf(
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )

        var permissionCamera = mutableListOf(
            Manifest.permission.CAMERA

        )

        var permissionCameraOld = mutableListOf(
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
        )

        var PHOTO_AND_GALLERY = 0

        val mensajes = arrayOf(
            "Habilita los permisos de cámara y archivos en la configuración de la aplicación, elige \"Permitir siempre\" o \"Solo con la app en uso\"",
            "Habilita los permisos de dispositivos cercanos en la configuración de la aplicación, elige \"Permitir siempre\" o \"Solo con la app en uso\"",
            "Habilita los permisos de ubicación en la configuración de la aplicación, elige \"Permitir siempre\" o \"Solo con la app en uso\""
        )

        fun msgPermisos(activity: Activity, type: Int) {
            if (!activity.isFinishing) {
                AlertDialog.Builder(activity).setTitle("Atención").setMessage(mensajes[type])
                    .setPositiveButton(
                        "Aceptar"
                    ) { _, _ ->
                        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
                        val uri = Uri.fromParts("package", activity.packageName, null)
                        intent.data = uri
                        activity.startActivity(intent)
                        activity.finish()
                    }.setCancelable(false).show()
            }
        }
    }
}
