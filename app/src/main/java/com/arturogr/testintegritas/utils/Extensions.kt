package com.arturogr.testintegritas.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import java.io.File

fun String.stringToBitMap(context: Context): Bitmap {
    val file = File(this)
    return BitmapFactory.decodeFile(file.toString())
}