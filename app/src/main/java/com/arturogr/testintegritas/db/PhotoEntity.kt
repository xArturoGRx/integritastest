package com.arturogr.testintegritas.db

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class PhotoEntity(
    @PrimaryKey(autoGenerate = true) var id: Int? = 1,
    val photo: String,
    val date: String,
)
