package com.arturogr.testintegritas.db

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface PhotoDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(userEntity: PhotoEntity): Long

    @Query("SELECT * FROM PhotoEntity")
    suspend fun getAllPhotos(): List<PhotoEntity>
}